package ru.t1.gorodtsova.tm.listener.system;

import org.jetbrains.annotations.NotNull;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import ru.t1.gorodtsova.tm.dto.request.system.ApplicationAboutRequest;
import ru.t1.gorodtsova.tm.dto.response.system.ApplicationAboutResponse;
import ru.t1.gorodtsova.tm.event.ConsoleEvent;

@Component
public final class ApplicationAboutListener extends AbstractSystemListener {

    @NotNull
    private final String ARGUMENT = "-a";

    @NotNull
    private final String DESCRIPTION = "Show developer info";

    @NotNull
    private final String NAME = "about";

    @NotNull
    @Override
    public String getArgument() {
        return ARGUMENT;
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @Override
    @EventListener(condition = "@applicationAboutListener.getName() == #consoleEvent.name")
    public void handler(@NotNull final ConsoleEvent consoleEvent) {
        System.out.println("[ABOUT]");
        @NotNull final ApplicationAboutRequest request = new ApplicationAboutRequest();
        @NotNull final ApplicationAboutResponse response = systemEndpoint.getAbout(request);
        System.out.println("name: " + response.getName());
        System.out.println("email: " + response.getEmail());
    }

}
