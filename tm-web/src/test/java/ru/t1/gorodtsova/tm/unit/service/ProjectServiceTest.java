package ru.t1.gorodtsova.tm.unit.service;

import org.jetbrains.annotations.NotNull;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import ru.t1.gorodtsova.tm.api.service.IProjectService;
import ru.t1.gorodtsova.tm.config.DataBaseConfiguration;
import ru.t1.gorodtsova.tm.marker.UnitCategory;
import ru.t1.gorodtsova.tm.model.Project;
import ru.t1.gorodtsova.tm.util.UserUtil;

import java.util.UUID;

@WebAppConfiguration
@Category(UnitCategory.class)
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = {DataBaseConfiguration.class})
public class ProjectServiceTest {

    @NotNull
    private static final String USERNAME = "test";

    @NotNull
    private static final String USERPASSWORD = "test";

    @NotNull
    private final Project project1 = new Project(UUID.randomUUID().toString());

    @NotNull
    private final Project project2 = new Project(UUID.randomUUID().toString());

    @NotNull
    private final Project project3 = new Project(UUID.randomUUID().toString());

    @NotNull
    @Autowired
    private AuthenticationManager authenticationManager;

    @NotNull
    @Autowired
    private IProjectService projectService;

    @Before
    public void initTest() {
        @NotNull final UsernamePasswordAuthenticationToken token = new UsernamePasswordAuthenticationToken(USERNAME, USERPASSWORD);
        @NotNull final Authentication authentication = authenticationManager.authenticate(token);
        SecurityContextHolder.getContext().setAuthentication(authentication);
        projectService.saveByUserId(project1, UserUtil.getUserId());
        projectService.saveByUserId(project2, UserUtil.getUserId());
    }

    @After
    public void clean() {
        projectService.clearByUserId(UserUtil.getUserId());
    }

    @Test
    public void saveTest() {
        projectService.save(project3);
        Assert.assertEquals(project3.getName(), projectService.findById(project3.getId()).getName());
        project3.setName(UUID.randomUUID().toString());
        projectService.save(project3);
        Assert.assertEquals(project3.getName(), projectService.findById(project3.getId()).getName());
    }

    @Test
    public void findAllByUserIdTest() {
        Assert.assertEquals(2, projectService.findAllByUserId(UserUtil.getUserId()).size());
    }

    @Test
    public void findByIdAndUserIdTest() {
        Assert.assertNotNull(projectService.findByIdAndUserId(project1.getId(), UserUtil.getUserId()));
    }

    @Test
    public void clearAllByUserIdTest() {
        projectService.clearByUserId(UserUtil.getUserId());
        Assert.assertEquals(0, projectService.findAllByUserId(UserUtil.getUserId()).size());
    }

    @Test
    public void removeOneByUserIdTest() {
        projectService.removeByUserId(project2, UserUtil.getUserId());
        Assert.assertNull(projectService.findByIdAndUserId(project2.getId(), UserUtil.getUserId()));
    }

    @Test
    public void removeByIdAndUserIdTest() {
        projectService.removeByIdAndUserId(project1.getId(), UserUtil.getUserId());
        Assert.assertNull(projectService.findByIdAndUserId(project1.getId(), UserUtil.getUserId()));
    }

}
