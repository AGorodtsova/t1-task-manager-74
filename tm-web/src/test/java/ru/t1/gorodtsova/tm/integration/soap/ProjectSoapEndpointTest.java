package ru.t1.gorodtsova.tm.integration.soap;

import lombok.SneakyThrows;
import org.apache.cxf.helpers.CastUtils;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.*;
import org.junit.experimental.categories.Category;
import org.springframework.http.HttpHeaders;
import ru.t1.gorodtsova.tm.api.endpoint.IAuthEndpoint;
import ru.t1.gorodtsova.tm.api.endpoint.IProjectRestEndpoint;
import ru.t1.gorodtsova.tm.client.AuthSoapEndpointClient;
import ru.t1.gorodtsova.tm.client.ProjectSoapEndpointClient;
import ru.t1.gorodtsova.tm.marker.IntegrationCategory;
import ru.t1.gorodtsova.tm.model.Project;

import javax.xml.ws.BindingProvider;
import javax.xml.ws.handler.MessageContext;
import java.util.*;

@Category(IntegrationCategory.class)
public class ProjectSoapEndpointTest {

    @NotNull
    private static final String URL = "http://localhost:8080";

    @NotNull
    private static IAuthEndpoint authEndpoint;

    @NotNull
    private static IProjectRestEndpoint projectEndpoint;

    @NotNull
    private final Project project1 = new Project(UUID.randomUUID().toString());

    @NotNull
    private final Project project2 = new Project(UUID.randomUUID().toString());

    @NotNull
    private final Project project3 = new Project(UUID.randomUUID().toString());

    @BeforeClass
    @SneakyThrows
    public static void beforeClass() {
        authEndpoint = AuthSoapEndpointClient.getInstance(URL);
        Assert.assertTrue(authEndpoint.login("test", "test").getSuccess());
        projectEndpoint = ProjectSoapEndpointClient.getInstance(URL);
        @NotNull final BindingProvider authBindingProvider = (BindingProvider) authEndpoint;
        @NotNull final BindingProvider projectBindingProvider = (BindingProvider) projectEndpoint;
        Map<String, List<String>> headers = CastUtils.cast((Map) authBindingProvider.getResponseContext().get(MessageContext.HTTP_RESPONSE_HEADERS));
        if (headers == null) headers = new HashMap<String, List<String>>();
        @NotNull final Object cookieValue = headers.get(HttpHeaders.SET_COOKIE);
        @NotNull final List<String> cookies = (List<String>) cookieValue;
        headers.put("Cookie", Collections.singletonList(cookies.get(0)));
        projectBindingProvider.getRequestContext().put(MessageContext.HTTP_REQUEST_HEADERS, headers);
    }

    @Before
    public void init() {
        projectEndpoint.save(project1);
        projectEndpoint.save(project2);
    }

    @After
    @SneakyThrows
    public void clean() {
        projectEndpoint.deleteAll();
    }

    @AfterClass
    public static void logout() {
        authEndpoint.logout();
    }

    @Test
    @Category(IntegrationCategory.class)
    public void saveTest() {
        projectEndpoint.save(project3);
        Assert.assertNotNull(projectEndpoint.findById(project3.getId()));
    }

    @Test
    public void findAllTest() {
        Assert.assertEquals(2, projectEndpoint.findAll().size());
    }

    @Test
    public void findByIdTest() {
        @NotNull final String expected = project1.getId();
        @Nullable final Project project = projectEndpoint.findById(expected);
        Assert.assertNotNull(project);
        final String actual = project.getId();
        Assert.assertEquals(expected, actual);
    }

    @Test
    public void deleteAllTest() {
        projectEndpoint.deleteAll();
        Assert.assertNull(projectEndpoint.findAll());
    }

    @Test
    public void deleteByIdTest() {
        @NotNull final String id = project1.getId();
        projectEndpoint.deleteById(id);
        Assert.assertNull(projectEndpoint.findById(id));
    }

    @Test
    public void deleteTest() {
        projectEndpoint.delete(project1);
        Assert.assertNull(projectEndpoint.findById(project1.getId()));
    }

}
