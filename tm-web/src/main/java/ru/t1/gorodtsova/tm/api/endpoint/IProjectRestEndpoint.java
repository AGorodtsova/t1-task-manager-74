package ru.t1.gorodtsova.tm.api.endpoint;

import org.jetbrains.annotations.Nullable;
import org.springframework.web.bind.annotation.*;
import ru.t1.gorodtsova.tm.model.Project;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import java.util.List;

@WebService
@RequestMapping("api/projects")
public interface IProjectRestEndpoint {

    @Nullable
    @WebMethod
    @GetMapping("/findAll")
    List<Project> findAll();

    @Nullable
    @WebMethod
    @GetMapping("/findById/{id}")
    Project findById(
            @WebParam(name = "id", partName = "id")
            @PathVariable("id") String id
    );

    @WebMethod
    @PostMapping("/save")
    void save(
            @WebParam(name = "project", partName = "project")
            @RequestBody Project project
    );

    @WebMethod
    @GetMapping("/deleteAll")
    void deleteAll();

    @WebMethod
    @PostMapping("/deleteById/{id}")
    void deleteById(
            @WebParam(name = "id", partName = "id")
            @PathVariable("id") String id
    );

    @WebMethod
    @PostMapping("/delete")
    void delete(
            @WebParam(name = "project", partName = "project")
            @RequestBody Project project
    );

}
