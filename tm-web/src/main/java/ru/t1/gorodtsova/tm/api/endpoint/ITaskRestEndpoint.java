package ru.t1.gorodtsova.tm.api.endpoint;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.web.bind.annotation.*;
import ru.t1.gorodtsova.tm.model.Task;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import java.util.List;

@WebService
@RequestMapping("api/tasks")
public interface ITaskRestEndpoint {

    @Nullable
    @WebMethod
    @GetMapping("/findAll")
    List<Task> findAll();

    @Nullable
    @WebMethod
    @GetMapping("/findById/{id}")
    Task findById(
            @WebParam(name = "id", partName = "id")
            @PathVariable("id") String id
    );

    @NotNull
    @WebMethod
    @PostMapping("/save")
    void save(
            @WebParam(name = "id", partName = "id")
            @RequestBody Task task
    );

    @WebMethod
    @GetMapping("/deleteAll")
    void deleteAll();

    @WebMethod
    @PostMapping("/deleteById/{id}")
    void deleteById(
            @WebParam(name = "id", partName = "id")
            @PathVariable("id") String id
    );

    @WebMethod
    @PostMapping("/delete")
    void delete(
            @WebParam(name = "task", partName = "task")
            @RequestBody Task task
    );

}
