package ru.t1.gorodtsova.tm.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import ru.t1.gorodtsova.tm.api.repository.IUserRepository;
import ru.t1.gorodtsova.tm.api.service.IUserService;
import ru.t1.gorodtsova.tm.enumerated.RoleType;
import ru.t1.gorodtsova.tm.exception.user.*;
import ru.t1.gorodtsova.tm.model.Role;
import ru.t1.gorodtsova.tm.model.User;

import javax.transaction.Transactional;
import java.util.Collections;
import java.util.List;

@Service
public class UserService implements IUserService {

    @NotNull
    @Autowired
    private IUserRepository userRepository;

    @NotNull
    @Autowired
    private PasswordEncoder passwordEncoder;

    @Transactional
    public void create(
            @Nullable final String login,
            @Nullable final String password,
            @Nullable RoleType roleType) {
        if (login == null || login.isEmpty()) throw new LoginEmptyException();
        if (password == null || password.isEmpty()) throw new PasswordEmptyException();
        if (userRepository.existsByLogin(login)) throw new ExistsLoginException();
        if (roleType == null) throw new RoleEmptyException();
        @NotNull final String passwordHash = passwordEncoder.encode(password);
        @NotNull final User user = new User();
        user.setLogin(login);
        user.setPasswordHash(passwordHash);
        @NotNull final Role role = new Role(roleType);
        role.setUser(user);
        user.setRoles(Collections.singletonList(role));
        userRepository.saveAndFlush(user);
    }

    @Transactional
    public void save(@Nullable final User user) {
        if (user == null) throw new UserNotFoundException();
        userRepository.saveAndFlush(user);
    }

    @Nullable
    public List<User> findAll() {
        return userRepository.findAll();
    }

    @Nullable
    public User findOneById(@Nullable final String id) {
        return userRepository.findById(id).orElse(null);
    }

    @Nullable
    public User findByLogin(@Nullable final String login) {
        if (login == null || login.isEmpty()) throw new LoginEmptyException();
        return userRepository.findByLogin(login);
    }

    @Transactional
    public void remove(@Nullable final User user) {
        if (user == null) throw new UserNotFoundException();
        userRepository.delete(user);
    }

    @Transactional
    public void removeById(@Nullable final String id) {
        if (id == null || id.isEmpty()) throw new UserNotFoundException();
        userRepository.deleteById(id);
    }

    public boolean existsByLogin(@Nullable final String login) {
        if (login == null || login.isEmpty()) throw new LoginEmptyException();
        return userRepository.existsByLogin(login);
    }

}
