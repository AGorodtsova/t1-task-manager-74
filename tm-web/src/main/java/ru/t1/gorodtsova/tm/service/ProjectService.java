package ru.t1.gorodtsova.tm.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.t1.gorodtsova.tm.api.repository.IProjectRepository;
import ru.t1.gorodtsova.tm.api.service.IProjectService;
import ru.t1.gorodtsova.tm.exception.field.ProjectIdEmptyException;
import ru.t1.gorodtsova.tm.exception.field.UserIdEmptyException;
import ru.t1.gorodtsova.tm.model.Project;
import ru.t1.gorodtsova.tm.util.UserUtil;

import javax.persistence.EntityNotFoundException;
import java.util.List;

@Service
public class ProjectService implements IProjectService {

    @Autowired
    private IProjectRepository projectRepository;

    public void create() {
        @NotNull final Project project = new Project("New project " + System.currentTimeMillis(),
                "New project");
        project.setUserId(UserUtil.getUserId());
        projectRepository.save(project);
    }

    @Override
    @Transactional
    public void save(@Nullable final Project project) {
        if (project == null) throw new EntityNotFoundException();
        projectRepository.save(project);
    }

    @Override
    @Transactional
    public void saveByUserId(@Nullable final Project project, @Nullable final String userId) {
        if (project == null) throw new EntityNotFoundException();
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        project.setUserId(userId);
        projectRepository.save(project);
    }

    @Override
    @Transactional
    public void clear() {
        projectRepository.deleteAll();
    }

    @Override
    @Transactional
    public void clearByUserId(@Nullable final String userId) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        projectRepository.deleteAllByUserId(userId);
    }

    @Override
    @Nullable
    public List<Project> findAll() {
        return projectRepository.findAll();
    }

    @Override
    @Nullable
    public List<Project> findAllByUserId(@Nullable final String userId) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        return projectRepository.findAllByUserId(userId);
    }

    @Nullable
    @Override
    public Project findById(@Nullable final String id) {
        return projectRepository.findById(id).orElse(null);
    }

    @Override
    @Nullable
    public Project findByIdAndUserId(@Nullable final String id, @Nullable final String userId) {
        if (id == null || id.isEmpty()) throw new ProjectIdEmptyException();
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        return projectRepository.findByUserIdAndId(userId, id);
    }

    @Override
    @Transactional
    public void remove(@Nullable final Project project) {
        if (project == null) throw new EntityNotFoundException();
        projectRepository.delete(project);
    }

    @Override
    @Transactional
    public void removeByUserId(@Nullable final Project project, @Nullable final String userId) {
        if (project == null) throw new EntityNotFoundException();
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        projectRepository.deleteByUserIdAndId(userId, project.getId());
    }

    @Override
    @Transactional
    public void removeById(@Nullable final String id) {
        if (id == null || id.isEmpty()) throw new ProjectIdEmptyException();
        projectRepository.deleteById(id);
    }

    @Transactional
    public void removeByIdAndUserId(@Nullable final String id, @Nullable final String userId) {
        if (id == null || id.isEmpty()) throw new ProjectIdEmptyException();
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        projectRepository.deleteByUserIdAndId(userId, id);
    }

}
