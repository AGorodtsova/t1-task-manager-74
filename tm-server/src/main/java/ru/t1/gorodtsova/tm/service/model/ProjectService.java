package ru.t1.gorodtsova.tm.service.model;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.t1.gorodtsova.tm.api.repository.model.IProjectRepository;
import ru.t1.gorodtsova.tm.api.repository.model.IUserRepository;
import ru.t1.gorodtsova.tm.api.service.model.IProjectService;
import ru.t1.gorodtsova.tm.comparator.CreatedComparator;
import ru.t1.gorodtsova.tm.comparator.StatusComparator;
import ru.t1.gorodtsova.tm.enumerated.Status;
import ru.t1.gorodtsova.tm.exception.entity.ModelNotFoundException;
import ru.t1.gorodtsova.tm.exception.entity.UserNotFoundException;
import ru.t1.gorodtsova.tm.exception.field.*;
import ru.t1.gorodtsova.tm.model.Project;
import ru.t1.gorodtsova.tm.model.User;

import java.util.Comparator;
import java.util.List;
import java.util.Optional;

@Service
public final class ProjectService extends AbstractUserOwnedService<Project>
        implements IProjectService {

    @NotNull
    @Autowired
    private IProjectRepository repository;

    @NotNull
    @Autowired
    private IUserRepository userRepository;

    @NotNull
    @Override
    @SneakyThrows
    @Transactional
    public Project create(@Nullable final String userId, @Nullable final String name) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (name == null || name.isEmpty()) throw new NameEmptyException();
        @NotNull Project project = new Project();
        project.setName(name);
        return add(userId, project);
    }

    @NotNull
    @Override
    @SneakyThrows
    @Transactional
    public Project create(
            @Nullable final String userId,
            @Nullable final String name,
            @Nullable final String description
    ) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (name == null || name.isEmpty()) throw new NameEmptyException();
        if (description == null) throw new DescriptionEmptyException();
        @NotNull Project project = new Project();
        project.setName(name);
        project.setDescription(description);
        return add(userId, project);
    }

    @Nullable
    @Override
    @SneakyThrows
    public List<Project> findAll(@Nullable final String userId, @Nullable final Comparator<Project> comparator) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (comparator == null) return findAll(userId);
        return repository.findAllByUserIdWithSort(userId, getSortedColumn(comparator));
    }

    @NotNull
    @Override
    @SneakyThrows
    @Transactional
    public Project updateById(
            @Nullable final String userId,
            @Nullable final String id,
            @Nullable final String name,
            @Nullable final String description
    ) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        if (name == null || name.isEmpty()) throw new NameEmptyException();
        if (description == null || description.isEmpty()) throw new DescriptionEmptyException();
        @NotNull final Project project = findOneById(userId, id);
        project.setName(name);
        project.setDescription(description);
        repository.save(project);
        return project;
    }

    @NotNull
    @Override
    @SneakyThrows
    @Transactional
    public Project changeProjectStatusById(
            @Nullable final String userId,
            @Nullable final String id,
            @Nullable final Status status
    ) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        if (status == null) throw new StatusEmptyException();
        @NotNull final Project project = findOneById(userId, id);
        project.setStatus(status);
        repository.save(project);
        return project;
    }

    @NotNull
    @Override
    @Transactional
    public Project add(@Nullable final String userId, @Nullable final Project model) {
        if (userId == null || userId.isEmpty()) throw new IdEmptyException();
        if (model == null) throw new ModelNotFoundException();
        final Optional<User> user = userRepository.findById(userId);
        if (!user.isPresent()) throw new UserNotFoundException();
        model.setUser(user.get());
        repository.save(model);
        return model;
    }

    @NotNull
    @Override
    public List<Project> findAll(@Nullable final String userId) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        return repository.findByUserId(userId);
    }

    @NotNull
    @Override
    public Project findOneById(@Nullable final String userId, @Nullable final String id) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        @Nullable final Project model = repository.findOneByUserIdAndId(userId, id);
        if (model == null) throw new ModelNotFoundException();
        return model;
    }

    @Override
    @Transactional
    public void removeAll(@Nullable final String userId) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        repository.deleteByUserId(userId);
    }

    @Override
    @Transactional
    public void removeOne(@Nullable final String userId, @Nullable final Project model) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (model == null) throw new ModelNotFoundException();
        repository.delete(model);
    }

    @Override
    @Transactional
    public void removeOneById(@Nullable final String userId, @Nullable final String id) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        findOneById(userId, id);
        repository.deleteOneByUserIdAndId(userId, id);
    }

    @Override
    public boolean existsById(@Nullable final String userId, @Nullable final String id) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        return repository.existsByUserIdAndId(userId, id);
    }

    @Override
    @SneakyThrows
    public long getSize(@Nullable final String userId) {
        if (userId == null || userId.isEmpty()) throw new IdEmptyException();
        return repository.countByUserId(userId);
    }

    @NotNull
    private String getSortedColumn(@NotNull final Comparator comparator) {
        if (comparator == CreatedComparator.INSTANCE) return "created";
        else if (comparator == StatusComparator.INSTANCE) return "status";
        else return "name";
    }

}
