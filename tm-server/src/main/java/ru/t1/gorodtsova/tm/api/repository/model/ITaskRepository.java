package ru.t1.gorodtsova.tm.api.repository.model;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import ru.t1.gorodtsova.tm.model.Task;

import java.util.List;

@Repository
public interface ITaskRepository extends IUserOwnedRepository<Task> {

    @NotNull
    List<Task> findByUserId(@NotNull String userId);

    @Nullable
    Task findOneByUserIdAndId(@NotNull String userId, @NotNull String id);

    @NotNull
    @Query("SELECT t FROM Task t WHERE t.user = :userId ORDER BY :sortColumn")
    List<Task> findAllByUserIdWithSort(@NotNull @Param("userId") String userId, @NotNull @Param("sortColumn") String sortColumn);

    @NotNull
    List<Task> findAllByUserIdAndProjectId(@NotNull String userId, @NotNull String projectId);

    void deleteByUserId(@NotNull String userId);

    void deleteOneByUserIdAndId(@NotNull String userId, @NotNull String id);

    boolean existsByUserIdAndId(@NotNull String userId, @NotNull String id);

    long countByUserId(@NotNull String userId);

}
