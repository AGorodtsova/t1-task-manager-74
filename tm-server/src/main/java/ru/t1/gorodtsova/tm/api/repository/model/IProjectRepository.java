package ru.t1.gorodtsova.tm.api.repository.model;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import ru.t1.gorodtsova.tm.model.Project;

import java.util.List;

@Repository
public interface IProjectRepository extends IUserOwnedRepository<Project> {

    @NotNull
    List<Project> findByUserId(@NotNull String userId);

    @NotNull
    @Query("SELECT p FROM Project p WHERE p.user = :userId ORDER BY :sortColumn")
    List<Project> findAllByUserIdWithSort(@NotNull @Param("userId") String userId, @NotNull @Param("sortColumn") String sortColumn);

    @Nullable
    Project findOneByUserIdAndId(@NotNull String userId, @NotNull String id);

    void deleteByUserId(@NotNull String userId);

    void deleteOneByUserIdAndId(@NotNull String userId, @NotNull String id);

    boolean existsByUserIdAndId(@NotNull String userId, @NotNull String id);

    long countByUserId(@NotNull String userId);

}
