package ru.t1.gorodtsova.tm.service.model;

import ru.t1.gorodtsova.tm.api.service.model.IUserOwnedService;
import ru.t1.gorodtsova.tm.model.AbstractUserOwnedModel;

public abstract class AbstractUserOwnedService<M extends AbstractUserOwnedModel>
        extends AbstractService<M> implements IUserOwnedService<M> {

}
