package ru.t1.gorodtsova.tm.api.service.dto;

import ru.t1.gorodtsova.tm.dto.model.SessionDTO;

public interface ISessionDtoService extends IUserOwnedDtoService<SessionDTO> {
}
