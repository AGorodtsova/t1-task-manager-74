package ru.t1.gorodtsova.tm.api.service;

import org.jetbrains.annotations.Nullable;
import ru.t1.gorodtsova.tm.model.Project;

import java.util.List;

public interface IProjectService {

    void create();

    void save(@Nullable Project model);

    void saveByUserId(@Nullable Project project, @Nullable String userId);

    void clear();

    void clearByUserId(@Nullable String userId);

    @Nullable
    List<Project> findAll();

    @Nullable
    List<Project> findAllByUserId(@Nullable String userId);

    @Nullable
    Project findById(@Nullable String id);

    @Nullable
    Project findByIdAndUserId(@Nullable String id, @Nullable String userId);

    void remove(@Nullable Project model);

    void removeByUserId(@Nullable Project project, @Nullable String userId);

    void removeById(@Nullable String id);

    void removeByIdAndUserId(@Nullable String id, @Nullable String userId);

}
