package ru.t1.gorodtsova.tm.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.t1.gorodtsova.tm.api.repository.ITaskRepository;
import ru.t1.gorodtsova.tm.api.service.ITaskService;
import ru.t1.gorodtsova.tm.exception.field.TaskIdEmptyException;
import ru.t1.gorodtsova.tm.exception.field.UserIdEmptyException;
import ru.t1.gorodtsova.tm.model.Task;
import ru.t1.gorodtsova.tm.util.UserUtil;

import javax.persistence.EntityNotFoundException;
import java.util.List;

@Service
public class TaskService implements ITaskService {

    @Autowired
    private ITaskRepository taskRepository;

    public void create() {
        @NotNull final Task task = new Task("New task " + System.currentTimeMillis());
        task.setUserId(UserUtil.getUserId());
        taskRepository.save(task);
    }

    @Override
    @Transactional
    public void save(@Nullable final Task task) {
        if (task == null) throw new EntityNotFoundException();
        task.setUserId(UserUtil.getUserId());
        taskRepository.save(task);
    }

    @Override
    @Transactional
    public void saveByUserId(@Nullable final Task task, @Nullable final String userId) {
        if (task == null) throw new EntityNotFoundException();
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        task.setUserId(userId);
        taskRepository.save(task);
    }


    @Override
    @Transactional
    public void clear() {
        taskRepository.deleteAll();
    }

    @Override
    @Transactional
    public void clearByUserId(@Nullable final String userId) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        taskRepository.deleteAllByUserId(userId);
    }

    @Override
    @Nullable
    public List<Task> findAll() {
        return taskRepository.findAll();
    }

    @Override
    @Nullable
    public List<Task> findAllByUserId(@Nullable final String userId) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        return taskRepository.findAllByUserId(userId);
    }

    @Nullable
    @Override
    public Task findById(@Nullable final String id) {
        if (id == null || id.isEmpty()) throw new TaskIdEmptyException();
        return taskRepository.findById(id).orElse(null);
    }

    @Override
    @Nullable
    public Task findByIdAndUserId(@Nullable final String id, @Nullable final String userId) {
        if (id == null || id.isEmpty()) throw new TaskIdEmptyException();
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        return taskRepository.findByIdAndUserId(id, userId);
    }

    @Override
    @Transactional
    public void remove(@Nullable final Task task) {
        if (task == null) throw new EntityNotFoundException();
        taskRepository.delete(task);
    }

    @Override
    @Transactional
    public void removeByUserId(@Nullable final Task task, @Nullable final String userId) {
        if (task == null) throw new EntityNotFoundException();
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        taskRepository.deleteByIdAndUserId(task.getId(), userId);
    }

    @Override
    @Transactional
    public void removeById(@Nullable final String id) {
        if (id == null || id.isEmpty()) throw new TaskIdEmptyException();
        taskRepository.deleteById(id);
    }

    @Override
    @Transactional
    public void removeByIdAndUserId(@Nullable final String id, @Nullable final String userId) {
        if (id == null || id.isEmpty()) throw new TaskIdEmptyException();
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        taskRepository.deleteByIdAndUserId(id, userId);
    }

}
