package ru.t1.gorodtsova.tm.exception.user;

public final class RoleEmptyException extends AbstractUserException {

    public RoleEmptyException() {
        super("Error! User's role is empty...");
    }

}
