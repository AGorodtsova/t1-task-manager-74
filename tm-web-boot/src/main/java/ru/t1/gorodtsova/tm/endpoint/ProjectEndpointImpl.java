package ru.t1.gorodtsova.tm.endpoint;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import ru.t1.gorodtsova.tm.api.endpoint.IProjectEndpoint;
import ru.t1.gorodtsova.tm.api.service.IProjectService;
import ru.t1.gorodtsova.tm.model.Project;
import ru.t1.gorodtsova.tm.util.UserUtil;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import java.util.List;

@RestController
@RequestMapping("/api/projects")
@WebService(endpointInterface = "ru.t1.gorodtsova.tm.api.endpoint.IProjectEndpoint")
public class ProjectEndpointImpl implements IProjectEndpoint {

    @Autowired
    private IProjectService projectService;

    @WebMethod
    @GetMapping("/findAll")
    public List<Project> findAll() {
        return projectService.findAllByUserId(UserUtil.getUserId());
    }

    @WebMethod
    @GetMapping("/findById/{id}")
    public Project findById(
            @WebParam(name = "id", partName = "id")
            @PathVariable("id") String id
    ) {
        return projectService.findByIdAndUserId(id, UserUtil.getUserId());
    }

    @WebMethod
    @PostMapping("/save")
    public void save(
            @WebParam(name = "project", partName = "project")
            @RequestBody Project project
    ) {
        projectService.saveByUserId(project, UserUtil.getUserId());
    }

    @WebMethod
    @PostMapping("/deleteAll")
    public void deleteAll() {
        projectService.clearByUserId(UserUtil.getUserId());
    }

    @WebMethod
    @PostMapping("/deleteById/{id}")
    public void deleteById(
            @WebParam(name = "id", partName = "id")
            @PathVariable("id") String id
    ) {
        projectService.removeByIdAndUserId(id, UserUtil.getUserId());
    }

    @WebMethod
    @PostMapping("/delete")
    public void delete(
            @WebParam(name = "project", partName = "project")
            @RequestBody Project project
    ) {
        projectService.removeByUserId(project, UserUtil.getUserId());
    }

}
